import json
from pathlib import Path
from shutil import rmtree, copy2

from phantom_dev.app import App
from phantom_dev.client import create, DEFAULT_CONNECTOR


def test_app():
	app_path = Path(__file__).absolute().parent.joinpath('dummy_app')
	copy2(DEFAULT_CONNECTOR, app_path)
	app = App(app_path)
	data = app.get_json()
	print(json.dumps(data, indent=4))
	return app


def test_build():
	app = test_app()
	build_dir = Path(__file__).parent.joinpath('test_build')
	build_dir.mkdir(exist_ok=True)
	app.build(directory=build_dir)


if __name__ == '__main__':
	from logging import basicConfig
	basicConfig(level='DEBUG')
	test_build()
